from prometheus_client import start_http_server, Summary
import random
import time

# Create a metric to track time spent and requests made.
REQUEST_TIME = Summary('request_processing_seconds', 'Time spent processing request')
WS_MICHI = Summary('ws_michi', 'Time spent processing request')
WS_HEISE = Summary('ws_heise', 'Time spent processing request')

# Decorate function with metric.
@REQUEST_TIME.time()
def process_request(t):
    """A dummy function that takes some time."""
    time.sleep(t)

@WS_MICHI.time()
def process_request_michi(t):
    """A dummy function that takes some time."""
    time.sleep(t)    

@WS_HEISE.time()
def process_request_heise(t):
    """A dummy function that takes some time."""
    time.sleep(t)       

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate some requests.
    while True:
        process_request(random.random())
        process_request_michi(random.random())
        process_request_heise(random.random())
