# Prometheus Python Service

Taken from https://github.com/prometheus/client_python#three-step-demo to practice for own app development.

Check the [blog post](https://dnsmichi.at/2021/09/14/monitoring-kubernetes-with-prometheus-and-grafana-free-workshop/) for more insights and resources for the Kubernetes Monitoring workshop.

## Practice Steps

- Fork the repository. Remove the fork relation in the settings.
- Run CI/CD to build and tag a Docker image.
- Inspect the image at `Packages & Registries > Container Registry` and copy the latest tag URL.
- Use the provided Kubernetes deployment manifests.

### Modifications

- Change the exported metrics, commit, merge MR to main and wait for the build to continue.
- Re-deploy the app to your Kubernetes cluster.

## Deployment

Edit the Deployment with the correct `image` URL.

```
$ vim ./manifests/ecc-python-service.yml
```

Create Deployment and Service in Kubernetes.

```
$ kubectl create -f ./manifests/ecc-python-service.yml
```

Create the ServiceMonitor object for Prometheus to auto-discover.

```
$ kubectl create -f ./manifests/ecc-python-service-monitor.yml
```

Open the `/targets` URL in Prometheus and verify the newly discovered targets.

### Modifications

Re-deploy the app to your Kubernetes cluster.

```
$ kubectl apply -f ./manifests/ecc-python-service.yml
$ kubectl apply -f ./manifests/ecc-python-service-monitor.yml
```

## Development

Open http://localhost:8000 to access the metrics.

On MacOS with Python 3 from Homebrew:

```
$ pip3 install -r requirements.txt

$ python3 app.py
```

With Docker pulling the CI built image:

```
$ docker run -p 8000:8000 -ti registry.gitlab.com/everyonecancontribute/observability/prometheus_python_service:latest
```
